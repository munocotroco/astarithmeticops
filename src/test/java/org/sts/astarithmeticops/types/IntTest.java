package org.sts.astarithmeticops.types;

import junit.framework.Assert;

public class IntTest {
    @org.junit.Test
    public void testEvaluate() throws Exception {
        Int v = new Int(10);
        Assert.assertEquals(new Int(10), v.evaluate());
    }

    @org.junit.Test
    public void testGetNumber() throws Exception {
        Int v = new Int(10);
        Assert.assertEquals(10, v.getNumber());
    }
}
