package org.sts.astarithmeticops.operations;

import junit.framework.Assert;
import org.junit.Test;
import org.sts.astarithmeticops.types.Int;
import org.sts.astarithmeticops.types.Value;

/**
 * Created with IntelliJ IDEA.
 * User: ALAYA
 * Date: 05-03-13
 * Time: 07:12 PM
 * To change this template use File | Settings | File Templates.
 */
public class AddTest {
    @Test
    public void testEvaluate() throws Exception {

        Value v1 = new Int(10);
        Value v2 = new Int(20);

        Assert.assertEquals(new Int(30), (new Add(v1, v2)).evaluate());

    }

    @Test
    public void testGetNumber() throws Exception {

        Value v1 = new Int(10);
        Value v2 = new Int(20);

        Assert.assertEquals(30, (new Add(v1, v2)).getNumber());

    }

}
