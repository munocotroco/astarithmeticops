package org.sts.astarithmeticops.operations;

import junit.framework.Assert;
import org.junit.Test;
import org.sts.astarithmeticops.types.Int;
import org.sts.astarithmeticops.types.Value;

public class MultiplicationTest {

    @Test
    public void testEvaluate() throws Exception {

        Value v1 = new Int(3);
        Value v2 = new Int(5);
        Assert.assertEquals(new Int(20), (new Multiplication(v1, v2)).evaluate());

    }

    @Test
    public void testGetNumber() throws Exception {

        Assert.fail("incomplete test");

    }
}
