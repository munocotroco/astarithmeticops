package org.sts.astarithmeticops.operations;

import junit.framework.Assert;
import org.junit.Test;
import org.sts.astarithmeticops.types.Int;
import org.sts.astarithmeticops.types.Value;

/**
 * Created with IntelliJ IDEA.
 * User: ALAYA
 * Date: 06-03-13
 * Time: 10:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class SubtractTest {
    @Test
    public void testEvaluate() throws Exception {

        Value v1 = new Int(20);
        Value v2 = new Int(15);
        Assert.assertEquals(new Int(5), (new Subtract(v1, v2)).evaluate());

    }

    @Test
    public void testGetNumber() throws Exception {

        Value v1 = new Int(20);
        Value v2 = new Int(15);
        Assert.assertEquals(5, (new Subtract(v1, v2).evaluate().getNumber()));

    }

}
