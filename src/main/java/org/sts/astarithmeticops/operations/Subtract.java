package org.sts.astarithmeticops.operations;

import org.sts.astarithmeticops.types.Int;
import org.sts.astarithmeticops.types.Value;

public class Subtract implements Value {

    private Value v1;

    private Value v2;

    public Subtract(Value v1, Value v2){
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public Value evaluate() {
        return new Int(this.v1.evaluate().getNumber().intValue()
            - this.v2.evaluate().getNumber().intValue());
    }

    @Override
    public Number getNumber() {
        return this.evaluate().getNumber();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Subtract subtract = (Subtract) o;

        if (v1 != null ? !v1.equals(subtract.v1) : subtract.v1 != null) return false;
        if (v2 != null ? !v2.equals(subtract.v2) : subtract.v2 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = v1 != null ? v1.hashCode() : 0;
        result = 31 * result + (v2 != null ? v2.hashCode() : 0);
        return result;
    }

}
