package org.sts.astarithmeticops.operations;

import org.sts.astarithmeticops.types.Int;
import org.sts.astarithmeticops.types.Value;

/**
 * Created with IntelliJ IDEA.
 * User: ALAYA
 * Date: 06-03-13
 * Time: 10:44 AM
 * To change this template use File | Settings | File Templates.
 */
public class Multiplication implements Value {

    private Value v1;

    private Value v2;

    public Multiplication(Value v1, Value v2){
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public Value evaluate() {
        return new Int(this.v1.evaluate().getNumber().intValue()
                * this.v2.evaluate().getNumber().intValue());
    }

    @Override
    public Number getNumber() {
        return this.evaluate().getNumber();
    }
}
