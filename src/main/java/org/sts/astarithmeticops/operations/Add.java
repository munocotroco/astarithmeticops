package org.sts.astarithmeticops.operations;

import org.sts.astarithmeticops.types.Int;
import org.sts.astarithmeticops.types.Value;

public class Add implements Value{

    private Value v1;

    private Value v2;

    public Add(Value v1, Value v2){
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public Value evaluate() {
        this.v1 = v1.evaluate();
        this.v2 = v2.evaluate();
        return new Int(this.v1.getNumber().intValue() + this.v2.getNumber().intValue());
    }

    @Override
    public Number getNumber() {
        return this.evaluate().getNumber();
    }
}
