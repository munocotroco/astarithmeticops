package org.sts.astarithmeticops.types;

public interface Value {

    public Value evaluate();

    public Number getNumber();

}
