package org.sts.astarithmeticops.types;

public class Int implements Value{

    private int val;

    public Int(int val){
        this.val = val;
    }

    @Override
    public Value evaluate() {
        return this;
    }

    @Override
    public Number getNumber() {
        System.out.println("In getNumber()");
        return new Integer(val);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Int anInt = (Int) o;

        if (val != anInt.val) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return val;
    }

}
